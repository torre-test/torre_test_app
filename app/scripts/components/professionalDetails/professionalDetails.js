import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Navbar from '../navbar/navbar';

const ProfessionalDetails = () => {
  const [professional, setProfessional] = useState([]);

  const { username } = useParams();

  useEffect(() => {
    const getProfessional = async () => {
      const response = await axios({
        method: 'GET',
        url: `http://127.0.0.1:3030/bio/${username}`,
      });

      setProfessional({
        person: response.data.person,
        education: response.data.education,
        jobs: response.data.jobs,
        languages: response.data.languages,
        strengths: response.data.strengths,
      })
    }
    getProfessional();
  }, [])

  return professional.length === 0 ? <h1>Loading</h1> : (
    <div className="professional-details">
      <div className="hero" style={{ backgroundImage: `url(${professional.person.picture})` }}>
        <Navbar />
      </div>
      <div className="professional-info">
        <h1>{professional.person.name}</h1>
        <h2>{professional.person.professionalHeadline}</h2>

        <div className="professional-info__detail">{professional.person.summaryOfBio}</div>
        <div className="professional-info__container">
          {
            professional.education && (
              <>
                <p>Education:</p>
                <ul>
                  {
                    professional.education.map(edu => (
                      <>
                        <li>
                          <p>Name: {edu.name}</p>
                          {
                            edu.organizations.length > 0 && (
                              <p>Organization: {edu.organizations[0].name}</p>
                            )
                          }
                          <p>From: {edu.fromMonth} {edu.fromYear}</p>
                          <p>To: {edu.toMonth} {edu.toYear}</p>
                        </li>
                      </>
                    ))
                  }
                </ul>
                <br />
              </>
            )
          }
          {
            professional.jobs && (
              <>
                <p>Jobs:</p>
                <ul>
                  {
                    professional.jobs.map(job => (
                      <>
                        <li>
                          <p>Name: {job.name}</p>
                          {
                            job.organizations.length > 0 && (
                              <p>Organization: {job.organizations[0].name}</p>
                            )
                          }
                          {
                            job.responsabilities?.map(respon => (
                              <p>{respon}</p>
                            ))
                          }
                          <p>From: {job.fromMonth} {job.fromYear}</p>
                          <p>To: {job.toMonth} {job.toYear}</p>
                        </li>
                      </>
                    ))
                  }
                </ul>
                <br />
              </>
            )
          }
          <p>Languages:</p>
          {
            <ul>
              {
                professional.languages.map(lang => (
                  <li>{lang.language} / {lang.fluency}</li>
                ))
              }
            </ul>
          }
        </div>
        <br />
        <div className="strengths-container">
          <p>Strengths:</p>
          {
            professional.strengths.map(str => (
              <span>{str.name}</span>
            ))
          }
        </div>
      </div>
    </div>
  );
};

export default ProfessionalDetails;
