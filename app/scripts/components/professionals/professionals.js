import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Navbar from '../navbar/navbar';
import Professional from '../professional/professional';

const Professionals = () => {
  const [state, setState] = useState({
    actualPage: 1,
    size: 20,
    professionals: [],
    total: 0,
  });

  const [formData, setFormData] = useState({});

  const goTo = e => {
    let newPage = state.actualPage;
    if (e.target.innerHTML === 'Prev') {
      newPage--;
      setState({
        ...state,
        actualPage: newPage,
      });
    }
    if (e.target.innerHTML === 'Next') {
      newPage++
      setState({
        ...state,
        actualPage: newPage,
      });
    }
  }

  const clearField = e => {
    const radioButtons = e.target.parentElement.children;

    for (let i = 1; i < radioButtons.length - 2; i += 2) {
      radioButtons[i].checked = false;
    }
    delete formData[radioButtons[1].name]
  };

  const handleChange = e => {
    const element = e.target;
    let value;

    if (element.name === "remoter") {
      value = element.value === "yes"

      setFormData({
        ...formData,
        [element.name]: value,
      });
    } else {
      setFormData({
        ...formData,
        [element.name]: element.value,
      });
    }
  }

  const search = async e => {
    e && e.preventDefault();
    const { actualPage, size } = state;
    const offset = (actualPage - 1) * size;
    const hasData = Object.keys(formData).length > 0;
    let response;

    response = await axios({
      method: 'POST',
      url: `http://127.0.0.1:3030/people/search?size=${size}&offset=${offset}`,
      data: hasData ? formData : [],
    });

    setState({
      ...state,
      professionals: response.data.results.map(professional => ({
        username: professional.username,
        location: professional.locationName,
        name: professional.name,
        openTo: professional.openTo,
        picture: professional.picture,
        skills: professional.skills,
        professionalHeadline: professional.professionalHeadline,
      })),
      total: response.data.total,
    })
  }

  useEffect(() => {
    search();
    window.scrollTo(0, 0)
  }, [state.size, state.actualPage])

  return state.professionals.length === 0 ? <h1>Loading</h1> : (
    <div className="professionals">
      <div className="hero">
        <Navbar />
        <h1>Professionals</h1>
      </div>
      <div className="search">
        <form className="primary" onSubmit={search}>
          <input type="text" name="name" id="name" placeholder="Search by name" onChange={handleChange} />
          <button type="button" onClick={search}>Search</button>
        </form>
      </div>
      <div className="professionals-container">
        {
          state.professionals.map(professional => (
            <Professional
              username={professional.username}
              picture={professional.picture}
              name={professional.name}
              professionalHeadline={professional.professionalHeadline}
              location={professional.location}
              openTo={professional.openTo}
              skills={professional.skills}
            />
          ))
        }
      </div>
      <div className="actual-page">
        <p>Page {state.actualPage}</p>
      </div>
      <div className="pages">
        <button type="button" onClick={goTo} disabled={state.actualPage === 1}>Prev</button>
        <button type="button" onClick={goTo} disabled={state.actualPage === Math.ceil(state.total / state.size)}>Next</button>
      </div>
    </div>
  );
};

export default Professionals;
