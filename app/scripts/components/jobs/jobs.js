import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Job from '../job/job';
import Navbar from '../navbar/navbar';

const Jobs = () => {
  const [state, setState] = useState({
    actualPage: 1,
    search_type: "",
    size: 20,
    jobs: [],
    total: 0,
  });

  const [formData, setFormData] = useState({});
  const [show, setShow] = useState({
    searchArea: "none",
    button: "display"
  });

  const goTo = e => {
    let newPage = state.actualPage;
    if (e.target.innerHTML === 'Prev') {
      newPage--;
      setState({
        ...state,
        actualPage: newPage,
      });
    }

    if (e.target.innerHTML === 'Next') {
      newPage++
      setState({
        ...state,
        actualPage: newPage,
      });
    }
  }

  const toggleAdvanceSearch = e => {
    const text = e.target;
    if (show.searchArea === "none") {
      setShow({
        searchArea: "inline",
        button: "none",
      });
      text.innerHTML = 'Less Options -'
    } else {
      setShow({
        searchArea: "none",
        button: "inline",
      });
      text.innerHTML = 'More Options +'
    }
  };

  const handleChange = e => {
    const element = e.target;
    let value;

    if (element.name === "remote") {
      value = element.value === "yes"

      setFormData({
        ...formData,
        [element.name]: value,
      });
    } else {
      setFormData({
        ...formData,
        [element.name]: element.value,
      });
    }
  };

  const search = async e => {
    e && e.preventDefault();
    const { actualPage, size } = state;
    const offset = (actualPage - 1) * size;
    const hasData = Object.keys(formData).length > 0;
    let response;

    response = await axios({
      method: 'POST',
      url: `http://127.0.0.1:3030/opportunities/search?size=${size}&offset=${offset}`,
      data: hasData ? formData : [],
    });

    setState({
      ...state,
      jobs: response.data.results.map(job => ({
        id: job.id,
        title: job.objective,
        organization: job.organizations,
        skills: job.skills,
        type: job.type,
        status: job.status,
        deadline: job.deadline,
        locations: job.locations,
        compensation: job.compensation,
      })),
      total: response.data.total,
    })
  }

  useEffect(() => {
    search();
    window.scrollTo(0, 0)
  }, [state.size, state.actualPage])

  return (
    <div className="jobs">
      <div className="hero">
        <Navbar />
        <h1>Opportunities</h1>
      </div>
      <div className="search">
        <form className="primary" onSubmit={search}>
          <input type="text" name="skill/role" id="skill" placeholder="Search by skill" onChange={handleChange} />
          <button type="button" onClick={search}>Search</button>
        </form>
      </div>
      <div className="jobs-container">
        {
          state.jobs?.map(job => (
            <Job
              title={job.title}
              imgSrc={job.organization[0]?.picture}
              organizationName={job.organization[0]?.name}
              skills={job.skills}
              location={job.locations[0]}
              type={job.type}
              deadline={job.deadline}
              compensation={job.compensation}
              id={job.id}
            />
          ))
        }
      </div>
      <div className="actual-page">
        <p>Page {state.actualPage}</p>
      </div>
      <div className="pages">
        <button type="button" onClick={goTo} disabled={state.actualPage === 1}>Prev</button>
        <button type="button" onClick={goTo} disabled={state.actualPage === Math.ceil(state.total / state.size)}>Next</button>
      </div>
    </div>
  );
};

export default Jobs;
