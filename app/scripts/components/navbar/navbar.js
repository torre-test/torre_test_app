import React from 'react';
import { Link } from 'react-router-dom';
// import './style.scss'

const Navbar = () => (
  <nav className="navbar">
    <Link to="/">
      <div className="logo">
        <h1>Torre</h1>
      </div>
    </Link>
  </nav>
);

export default Navbar;
