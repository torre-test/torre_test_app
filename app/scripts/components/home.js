import React from 'react';
import Navbar from './navbar/navbar';
import { Link } from 'react-router-dom';

const Home = () => (
  <div className="home">
    <div className="card__first">
      <Navbar />
      <div className="content">
        <h1>
          Are you a <span>looking</span> for a job <span>opportunity?</span>
        </h1>
        <Link to="/jobs" className="button">
          Start now
        </Link>
      </div>
    </div>
    <div className="card__second">
      <h1>
        Looking for a <span>qualified professional</span>?
      </h1>
      <Link to="/professionals" className="button">
        Start now
      </Link>
    </div>
  </div>
);

export default Home;