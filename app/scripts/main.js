/**
 * The Initial React Setup file.
 */
import "@babel/polyfill";

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Home from './components/home';
import Professionals from './components/professionals/professionals';
import ProfessionalDetails from './components/professionalDetails/professionalDetails';
import Jobs from './components/jobs/jobs';
import JobDetails from './components/jobDetails/jobDetails';

/**
 * We can start our initial App here in the main.js file
 */
class App extends React.Component {

    /**
     * Renders the default app in the window, we have assigned this to an element called root.
     * 
     * @returns JSX
     * @memberof App
    */
    render() {
        return (
            <div className="App">
                <Router>
                    <Switch>
                        <Route exact path='/'>
                            <Home />
                        </Route>
                        <Route path="/professionals/:username">
                            <ProfessionalDetails />
                        </Route>
                        <Route exact path='/professionals'>
                            <Professionals />
                        </Route>
                        <Route path="/jobs/:id">
                            <JobDetails />
                        </Route>
                        <Route exact path='/jobs'>
                            <Jobs />
                        </Route>
                    </Switch>
                </Router>
            </div>
        );
    }

}

// Render this out
ReactDOM.render(<App />, document.getElementById('root'));