/**
 * Gulp file used for creating front end templates on local instances.
 */
'use strict';

// Configs
// Webpack Configs
const webpack_config_dev = require('./config/webpack.config.js');
const template_config = require('./config/template.data.js');

// The gulp task runner
const gulp = require('gulp');

// CSS
const sass = require('gulp-sass');
const prefix = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');

// SCSS
const sassLint = require('gulp-sass-lint');

//Node Server
const nodemon = require('nodemon');

// HTML Templating, we use mustache to ensure it works with the Live Drupal site
const mustache = require('gulp-mustache');

// Javasript - WebPack Version
const webpack = require('webpack-stream');
const uglify = require('gulp-uglifyjs');

// Browser Sync to auto refresh and keep things quick
const browserSync = require('browser-sync');

// General Use Packages
const fs  = require('fs');
const git = require('gulp-git');
const rename = require('gulp-rename');

// Global
// Can be set either Dynamically using a date stamp or set manually by overriding the below with a string.
const _serverFilesFolder = ".local_server";
let _logfile = "";

// Error handling
// Add the Error to the Browser Sync message so you can see it easily
function errorLog($err) {
  console.log($err);
  browserSync.notify('<span style="file-size: 22px;">Error : ' + $err + '</span>', 10000);
  this.emit('end');
}

function infoLog($message) {
  const ts = new Date();
  $message = '\n' + '[' + ts.getDate() + '/' + (ts.getMonth() + 1) + '/' + ts.getFullYear() + ' -- ' + ts.getHours() + ':' + ts.getMinutes() + ']  ' + (_logfile === "" ? 'Info task started' : $message);

  // grab the log file
  if(_logfile === "") {
    fs.readFile('./logs/.logfile.txt', 'utf8', function($err, $data) {
      if($err) {
        console.log('Error with logging file', $err);
      } else {
        // Store the data we have saved
        _logfile = $data;
        _logfile += $message;
        // Write with the new message
        fs.writeFile('./logs/.logfile.txt', _logfile, () => {
          // file logged
          return;
        });
      }
    });
  } else {
    // Append the new message
    _logfile += $message;

    // If git changes exist then also write these out
    git.exec({
      args: 'log', function($err, $out) {
        _logfile += "\n" + 'Git changes : ' + $out;
      }
    });

    // Write to file
    fs.writeFile('./logs/.logfile.txt', _logfile, () => {
      // file logged
      return;
    });
  }
}

// Sass Task Runner
gulp.task('sass', () => {
  // Log this change
  infoLog('Sass ran on files');

  // Return the task
  return gulp.src('./app/sass/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'compact', includePaths: ['./app/sass'] }))
    .on('error', errorLog)
    .pipe(prefix({
      browsers: ['last 2 versions']
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./' + _serverFilesFolder + '/css'))
    .pipe(browserSync.stream());
});

// Sass lint runner.
gulp.task('sass-linter', () => {

  // Returns the task.
    return gulp.src('./app/sass/**/*.s+(a|c)ss')
        .pipe(sassLint({}))
        .pipe(sassLint.format())
        .pipe(sassLint.failOnError())
});

//Node server task
gulp.task('server', (cb) => {
    let callbackCalled = false;

    return nodemon({script: './server/app.js'}).on('start', function () {
        if (!callbackCalled) {
            callbackCalled = true;
            cb();
        }
    });
});

// Scripts runner, this time using the Webpack Framework
gulp.task('scripts', () => {
  // Log this change
  infoLog('Scripts ran on main.js');

  // Return the task
  return gulp.src('./app/scripts/main.js')
    .pipe(webpack(
      webpack_config_dev
    ))
    .on('error', errorLog)
    .pipe(gulp.dest('./' + _serverFilesFolder + '/js/'))
    .on('error', errorLog)
    .pipe(browserSync.stream());
  
});

// Scripts runner, this time using the Webpack Framework
gulp.task('minify', () => {
  // Log this change
  infoLog('Minify ran on the JS file');

  // Return the task
  return gulp.src('./prod/js/app.min.js')
    .pipe(uglify())
    .on('error', errorLog)
    .pipe(gulp.dest('./' + _serverFilesFolder + '/js/'))
    .on('error', errorLog);
});

gulp.task('mustache', () => {
  // Log this change
  infoLog('Mustache Templates ran on the HTML files');

  // Data for the site can be loaded via a JSON file
  return gulp.src(['./app/views/*.mustache'])
    .on('error', errorLog)
    .pipe(
      mustache(template_config)
    )
    .on('error', errorLog)
    .pipe(rename(function(path){
      path.extname = ".html";
    }))
    .pipe(gulp.dest('./' + _serverFilesFolder + '/'))
    .on('error', errorLog)
    .pipe(browserSync.stream());

});

// Main task Runner for watching all the scripts, start by running through the tasks
// to ensure everything is up to date
gulp.task('watch', () => {
  // Run the browser sync on port 3030 (no conflicts)
  browserSync.init({
        server: './' + _serverFilesFolder,
        port: 3031,
  });
  gulp.watch('./app/**/*.scss', gulp.series('sass-linter', 'sass'));
  gulp.watch(['./app/scripts/**/*.jsx', './app/scripts/**/*.js'], gulp.series('scripts'));
  gulp.watch(['./app/**/*.mustache', './app/**/*.json', './app/**/*.html'], gulp.series('mustache'));
});

gulp.task('build', gulp.series('mustache', 'scripts', 'sass-linter', 'sass', 'server', 'watch'));