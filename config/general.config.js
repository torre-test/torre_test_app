"use strict";
/**
 * 
 * File is used to setup the General project
 *  
 */

module.exports = {

    development_folder: ".local_server",

    dev_port: 3030,

    base_url: 'http://localhost'
};