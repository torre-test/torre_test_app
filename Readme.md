# Torre Test App
This is a test app for the torre endpoints.

- GET https://torre.bio/api/bios/$username (gets bio information of $username)

- GET https://torre.co/api/opportunities/$id (gets job information of $id)

- POST https://search.torre.co/opportunities/_search/?offset=$offset&size=$size&aggregate=$aggregate (gets opportunities list)

- POST https://search.torre.co/people/_search/?offset=$offset&size=$size&aggregate=$aggregate (gets people in general).

## How to run the app
### Clone the project
```
$ git clone git@bitbucket.org:torre-test/torre_test_app.git
```

### Install dependencies
Inside the project root folder run:

```
$ npm install
```

### Run the app
This command will create the main.min.js file to serve.

```
$ npm run build
```