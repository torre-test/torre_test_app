const express    = require('express');
const bodyParser = require('body-parser');
const cors       = require('cors');
const app        = express();
const port       = 3030;

/**
 * Middlewares.
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

/**
 * Routes.
 */
app.use(require('./routes/index'));

/**
 * Server listener.
 */
app.listen(port, () => { console.log("WEB project is started to " + port + " port") });