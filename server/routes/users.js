/**
 * User routes.
 */
const express = require('express');
const fetch = require('node-fetch');

let app = express();

app.get('/bio/:username', async (req, res) => {
  const username = req.params.username;

  try {
    const response = await fetch(`https://torre.bio/api/bios/${username}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      }
    });
    
    res.status(200).send(await response.json());
  } catch (error) {
    res.status(400).send(error);
  }
});

app.post('/people/search', async (req, res) => {
  const payload = [];
  const size = req.query.size;
  const offset = req.query.offset;

  for (const key in req.body) {
    if (key === 'name') {
      payload.push({ [key]: { term: req.body[key] } });
    }
  }
  
  const bodyToSend = JSON.stringify({ and: payload });

  try {
    response = await fetch(`https://search.torre.co/people/_search/?size=${size}&offset=${offset}`, {
      method: 'POST',
      body: bodyToSend.length > 0 ? bodyToSend : [],
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
    });
  
    res.status(200).send(await response.json());
  } catch(error) {
    res.status(400).send(error);
  }
});

module.exports = app;