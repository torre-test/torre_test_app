/**
 * Index for routes.
 */
const express = require('express');
const app = express()

app.use(require('./users'));
app.use(require('./jobs'));

module.exports = app;