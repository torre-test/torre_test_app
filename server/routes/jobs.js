/**
 * Product routes.
 */
const express = require('express');
const fetch = require('node-fetch');

let app = express();

app.get('/opportunity/:id', async (req, res) => {
  const id = req.params.id;

  try {
    const response = await fetch(`https://torre.co/api/opportunities/${id}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
    });

    res.status(200).send(await response.json());
  } catch (error) {
    res.status(400).send(error);
  }
});

app.post('/opportunities/search', async (req, res) => {
  const payload = [];
  const size = req.query.size;
  const offset = req.query.offset;

  for (const key in req.body) {
    if (key === 'skill/role') {
      payload.push({ [key]: { text: req.body[key], experience: 'potential-to-develop' } });
    }
  }
  
  const bodyToSend = JSON.stringify({ and: payload });
  
  try {
    response = await fetch(`https://search.torre.co/opportunities/_search/?size=${size}&offset=${offset}`, {
      method: 'POST',
      body: bodyToSend.length > 0 ? bodyToSend : [],
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
    });

    res.status(200).send(await response.json());
  } catch(error) {
    res.status(400).send(error);
  }
});

module.exports = app;